Membrii echipei:
1. Cristian Guta (cristi98feb@yahoo.com)
2. Iulia Purcaru (iuliapurcaru25@gmail.com)

Cerinte implementate:
Implementat o operatie cu camera (facut poze sau filmat)
Implementat un Recycler View cu functie de cautare
Utilizat o metoda de navigatie: Bottom Navigation
Implementat o metoda de Share (Android Sharesheet)

UI adaptat pentru landscape mode
Video Playback sau video streaming
Social login (Facebook)

Link Confluence: https://test-java.atlassian.net/wiki/spaces/GIVEMEFIVE/pages/131073/GiveMeFive+application